$(document).ready(function(){

   let osmLayer = new ol.layer.Tile({
        source: new ol.source.OSM(),
        visible: true
    });

    let vectorLayer;

    let map = new ol.Map({
        target: 'mapPlaceholder',
        layers: [
            osmLayer
        ],
        view: new ol.View({
            //Somewhere in Germany
            center: ol.proj.fromLonLat([8.66, 48.88]),
            zoom: 5
        })
    });

    let pointStyle = new ol.style.Style({
        image: new ol.style.Circle({
            radius: 5,
            fill: null,
            stroke: new ol.style.Stroke({color: 'red', width: 1})
        })
    });
    let styleFunction = function(feature) {
        return pointStyle;
    };

    //TODO: add code here
    $('#list-all-airports').on('click', function() {
    console.log('pulling data');
    $('#airports').empty();
    $.getJSON('http://localhost:7070/json/german_airports.geojson', {}).done(function(json) {
        console.log(json);
    });
});
$('#list-all-airports').on('click', function() {
    console.log('pulling data');
    $('#airports').empty();
    $.getJSON('http://localhost:7070/json/german_airports.geojson', {}).done(function(json) {
        for (var i=0; i<json.features.length; i++) {
            $('#airports').append('<li>' + json.features[i].properties.dataField + '</li>');
        }
    });
});
let vectorSource = new ol.source.Vector({
    format: new ol.format.GeoJSON(),
    url: 'http://localhost:7070/json/german_airports.geojson'
});
vectorLayer = new ol.layer.Vector({
    source: vectorSource,
    style: styleFunction
});
map.addLayer(vectorLayer);
map.on('click', function(event) {
    $('#airports').empty();
    map.forEachFeatureAtPixel(event.pixel, function(feature,layer) {
        $('#airports').append(`
            <li>
                <h3>${feature.get('locationType')}</h3>
                <p>ICAO: ${feature.get('icao')}</p><p>Location: ${feature.get('dataField')}, ${feature.get('country')}</p>
                <p>Altitude: ${feature.get('alt')}</p>
                <p>Time Zone: ${feature.get('tz')}</p>
            </li>`
        );
    });
});
});
